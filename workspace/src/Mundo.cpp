//////////////////////////////////////////////////////////////////////
//////////////////////////autor:Javier Robles/////////////////////////
//////////////////////////////////////////////////////////////////////

// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <iostream>
using namespace std;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	
	for (int i=0; i<esferas.size(); i++)
		esferas[i]->Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	
	for (int i=0; i<esferas.size();i++)
		esferas[i]->Mueve(0.025f);
	
	int i;
	for(i=0;i<paredes.size();i++)
	{
		for (int j=0; j<esferas.size();j++)
			paredes[i].Rebota(*(esferas[j]));
		
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	for (int i=0; i<esferas.size(); i++){		//Sumamos en el contador cada rebote con la raqueta
		if(jugador1.Rebota(*(esferas[i])))
			esferas[i]->contador+=1;
		if(jugador2.Rebota(*(esferas[i])))
			esferas[i]->contador+=1;
	}
	
	for(int i=0; i<esferas.size();i++){		//Reducimos el radio 2/3 del actual cada vez que tenga mas de 5 rebotes
		if(esferas[i]->contador >=5){
			esferas[i]->radio=2*esferas[i]->radio/3;
			esferas[i]->contador=0;
			if(esferas.size()<3){		//Si tenemos menos de 3 esferas se crea una nueva
				esferas.push_back(new Esfera);
			}
			if(esferas[i]->radio<0.2f){	//Si alguna esfera tiene menor radio de 0.2 se elimina
				delete esferas[i];
				esferas.erase(esferas.begin()+i);
			}
		}
	}
	
	for(int i=0; i<esferas.size();i++){
		if(fondo_izq.Rebota(*(esferas[i])))
		{
			esferas[i]->centro.x=0;
			esferas[i]->centro.y=rand()/(float)RAND_MAX;
			esferas[i]->velocidad.x=2+2*rand()/(float)RAND_MAX;
			esferas[i]->velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;
		}
	
	}
	
	
	for(int i=0; i<esferas.size();i++){
		if(fondo_dcho.Rebota(*(esferas[i])))
		{	
			esferas[i]->centro.x=0;
			esferas[i]->centro.y=rand()/(float)RAND_MAX;
			esferas[i]->velocidad.x=-2-2*rand()/(float)RAND_MAX;
			esferas[i]->velocidad.y=-2-2*rand()/(float)RAND_MAX;
			puntos1++;
		}
	}

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
	//Raqueta 1 (la de la izquierda)
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;	//Dir Abajo
	case 'w':jugador1.velocidad.y=4;break;	//Dir Arriba
	//Raqueta 2 (la de la derecha)
	case 'l':jugador2.velocidad.y=-4;break;	//Dir Abajo
	case 'o':jugador2.velocidad.y=4;break;	//Dir Arriba

	}
}

void CMundo::Init()
{
	Plano p;
	esferas.push_back(new Esfera);		//Creacion de las esferas
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
