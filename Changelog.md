#Changelog
Los cambios mas notorios seran documentados en este documento

## 2021-11-09

### Añadido

- SRC
- Anadido el movimiento en la raqueta y la esfera en sus respectivos cpp
- Modificamos el cpp de Mundo para poder generar mas de una esfera simultanea, para ello, convertimos esfera en un vector de esferas; tambien anadimos las funciones para modificar el tamano y numero de esferas segun el numero de rebotes de la esfera con la raqueta.

- INCLUDE
- En esfera.h añado el contador para saber el numero de rebotes raqueta-esfera
- En mundo.h modificamos esfera a un vector esferas para permitir la generacion de varias

### Cambios

- Se han realizado cambios en Mundo.cpp / Esfera.cpp / Raqueta.cpp / Mundo.h / Esfera.h / Changelog.md / 

## 2021-10-08
### Añadido

- Documento .gitignore para poder ignorar todos los elementos que no queramos ir modificando el remoto


## 2021-10-07
### Añadido

- He añadido el documento Changelog

### Cambios

- Sin cambios notorios

### Eliminado

- Elimino la carpeta "build" 
